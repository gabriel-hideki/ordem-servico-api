package br.com.os.api.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.os.api.model.input.OrdemServicoInput;
import br.com.os.api.model.representation.OrdemServicoModel;
import br.com.os.domain.model.OrdemServico;
import br.com.os.domain.repository.OrdemServicoRepository;
import br.com.os.domain.service.GestaoOrdemServicoService;

@RestController
@RequestMapping("/ordens-servico")
public class OrdemServicoController {

	@Autowired
	private GestaoOrdemServicoService gestaoOrdemServico;

	@Autowired
	private OrdemServicoRepository ordemServicoRepository;

	@Autowired
	private ModelMapper modelMapper;

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public OrdemServicoModel criar(@Valid @RequestBody OrdemServicoInput ordemServicoInput) {
		 OrdemServico ordemServico = convertToEntity(ordemServicoInput);
		
		return convertToModel(gestaoOrdemServico.criar(ordemServico));
	}

	@GetMapping
	public List<OrdemServicoModel> listar() {
		List<OrdemServico> ordensServico = ordemServicoRepository.findAll();

		return convertToListModel(ordensServico);
	}

	@GetMapping("/{id}")
	public ResponseEntity<OrdemServicoModel> buscar(@PathVariable Long id) {
		Optional<OrdemServico> ordemServico = ordemServicoRepository.findById(id);

		if (ordemServico.isPresent()) {
			OrdemServicoModel ordemServicoModel = convertToModel(ordemServico.get());
			return ResponseEntity.ok(ordemServicoModel);
		}

		return ResponseEntity.notFound().build();
	}
	
	@PutMapping("/{id}/finalizacao")
	public ResponseEntity<Void> finalizar(@PathVariable Long id) {
		gestaoOrdemServico.finalizarOrdemServico(id);

		return ResponseEntity.noContent().build();
	}
	
	@GetMapping("/status/{status}")
	public List<OrdemServicoModel> buscarStatus(@PathVariable String status) {
		List<OrdemServico> listaDeOs = gestaoOrdemServico.buscarStatus(status.toUpperCase());
			
		return convertToListModel(listaDeOs);
	}

	private OrdemServicoModel convertToModel(OrdemServico ordemServico) {
		return modelMapper.map(ordemServico, OrdemServicoModel.class);
	}

	private List<OrdemServicoModel> convertToListModel(List<OrdemServico> ordensServico) {
		return ordensServico.stream().map(os -> convertToModel(os)).collect(Collectors.toList());
	}
	
	private OrdemServico convertToEntity(OrdemServicoInput ordemServicoInput) {
		return modelMapper.map(ordemServicoInput, OrdemServico.class);
	}

}
