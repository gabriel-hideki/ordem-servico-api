package br.com.os.domain.model;

public enum StatusOrdemServico {

	ABERTA, FINALIZADA, CANCELADA

}
