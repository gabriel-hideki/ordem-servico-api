package br.com.os.api.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.os.api.model.input.ClienteInput;
import br.com.os.api.model.representation.ClienteModel;
import br.com.os.domain.model.Cliente;
import br.com.os.domain.repository.ClienteRepository;
import br.com.os.domain.service.CadastroClienteService;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

	@Autowired
	private ClienteRepository clienteRepository;

	@Autowired
	private CadastroClienteService cadastroCliente;

	@Autowired
	private ModelMapper modelMapper;

	@GetMapping
	public Page<ClienteModel> listar(Pageable paginacao) {
		Page<Cliente> clientes = clienteRepository.findAll(paginacao);

		return convertToPageModel(clientes);
	}

	@GetMapping("/{id}")
	public ResponseEntity<ClienteModel> buscar(@PathVariable Long id) {
		Optional<Cliente> cliente = clienteRepository.findById(id);

		if (cliente.isPresent()) {
			return ResponseEntity.ok(convertToModel(cliente.get()));
		}

		return ResponseEntity.notFound().build();
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ClienteModel adicionar(@Valid @RequestBody ClienteInput clienteInput) {
		Cliente cliente = convertToEntity(clienteInput);
		return convertToModel(cadastroCliente.salvar(cliente));
	}

	@PutMapping("/{id}")
	public ResponseEntity<ClienteModel> atualizar(@PathVariable Long id, @Valid @RequestBody ClienteInput clienteInput) {
		if (!clienteRepository.existsById(id)) {
			return ResponseEntity.notFound().build();
		}

		clienteInput.setId(id);
		Cliente cliente = convertToEntity(clienteInput);
		
		cadastroCliente.salvar(cliente);

		return ResponseEntity.ok(convertToModel(cliente));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Void> remover(@PathVariable Long id) {
		if (!clienteRepository.existsById(id)) {
			return ResponseEntity.notFound().build();
		}

		cadastroCliente.remover(id);

		return ResponseEntity.noContent().build();
	}

	private Cliente convertToEntity(@Valid ClienteInput clienteInput) {
		return modelMapper.map(clienteInput, Cliente.class);
	}

	private ClienteModel convertToModel(Cliente cliente) {
		return modelMapper.map(cliente, ClienteModel.class);
	}
	
	private Page<ClienteModel> convertToPageModel(Page<Cliente> clientes) {
		return clientes.map(cliente -> convertToModel(cliente));
	}

}
