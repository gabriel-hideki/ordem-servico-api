package br.com.os.api.exception;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.os.api.exception.ApiException.Campo;
import br.com.os.domain.exception.EntidadeNaoEncontradaException;
import br.com.os.domain.exception.NegocioException;

@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	private MessageSource messageSource;

	@ExceptionHandler(NegocioException.class)
	public ResponseEntity<Object> handlerNegocio(NegocioException ex, WebRequest request) {
		HttpStatus status = HttpStatus.BAD_REQUEST;

		ApiException apiException = new ApiException();
		apiException.setStatus(status.value());
		apiException.setTitulo(ex.getMessage());
		apiException.setDataHora(OffsetDateTime.now());

		return handleExceptionInternal(ex, apiException, new HttpHeaders(), status, request);
	}

	@ExceptionHandler(EntidadeNaoEncontradaException.class)
	public ResponseEntity<Object> handlerEntidadeNaoEncontrada(EntidadeNaoEncontradaException ex, WebRequest request) {
		HttpStatus status = HttpStatus.NOT_FOUND;

		ApiException apiException = new ApiException();
		apiException.setStatus(status.value());
		apiException.setTitulo(ex.getMessage());
		apiException.setDataHora(OffsetDateTime.now());

		return handleExceptionInternal(ex, apiException, new HttpHeaders(), status, request);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		ApiException apiException = new ApiException();
		List<Campo> campos = new ArrayList<>();

		List<FieldError> fieldError = ex.getBindingResult().getFieldErrors();

		fieldError.forEach(erro -> {
			String mensagem = messageSource.getMessage(erro, LocaleContextHolder.getLocale());

			campos.add(new Campo(erro.getField(), mensagem));
		});

		apiException.setStatus(status.value());
		apiException.setTitulo("Um ou mais campos estão inválidos. Faça o preenchimento correto e tente novamente!");
		apiException.setDataHora(OffsetDateTime.now());
		apiException.setCampos(campos);

		return super.handleExceptionInternal(ex, apiException, headers, status, request);
	}

}
