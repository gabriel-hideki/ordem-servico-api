package br.com.os.api.model.representation;

import java.time.OffsetDateTime;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ComentarioModel {

	private Long id;
	private String descricao;
	private OffsetDateTime dataEnvio;

}
