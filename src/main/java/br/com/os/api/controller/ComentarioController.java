package br.com.os.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.os.api.model.input.ComentarioInput;
import br.com.os.api.model.representation.ComentarioModel;
import br.com.os.domain.exception.EntidadeNaoEncontradaException;
import br.com.os.domain.model.Comentario;
import br.com.os.domain.model.OrdemServico;
import br.com.os.domain.repository.OrdemServicoRepository;
import br.com.os.domain.service.GestaoOrdemServicoService;

@RestController
@RequestMapping("/ordens-servico/{ordemServicoId}/comentarios")
public class ComentarioController {

	@Autowired
	private GestaoOrdemServicoService comentarioService;

	@Autowired
	private OrdemServicoRepository ordemServicoRepository;

	@Autowired
	private ModelMapper modelMapper;

	@GetMapping
	public Page<ComentarioModel> listar(@PathVariable Long ordemServicoId, Pageable paginacao) {
		OrdemServico ordemServico = ordemServicoRepository.findById(ordemServicoId).orElseThrow(() -> new EntidadeNaoEncontradaException("Ordem de serviço não encontrada"));

		List<Comentario> comentarios = ordemServico.getComentarios();
		Page<Comentario> comentariosPage = new PageImpl<>(comentarios);

		return convertToPageModel(comentariosPage);
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public ComentarioModel adicionar(@Valid @RequestBody ComentarioInput comentarioInput, @PathVariable Long ordemServicoId) {
		Comentario comentario = comentarioService.adicionarComentario(ordemServicoId, comentarioInput.getDescricao());

		return convertToModel(comentario);
	}

	private ComentarioModel convertToModel(Comentario comentario) {
		return modelMapper.map(comentario, ComentarioModel.class);
	}

	private Page<ComentarioModel> convertToPageModel(Page<Comentario> comentarios) {
		return comentarios.map(comentario -> convertToModel(comentario));
	}

}
