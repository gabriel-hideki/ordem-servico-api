package br.com.os.domain.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.os.domain.model.OrdemServico;
import br.com.os.domain.model.StatusOrdemServico;

@Repository
public interface OrdemServicoRepository extends JpaRepository<OrdemServico, Long> {

	List<OrdemServico> findByStatus(StatusOrdemServico status);

}