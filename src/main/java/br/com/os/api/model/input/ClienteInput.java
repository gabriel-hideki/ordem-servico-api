package br.com.os.api.model.input;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import br.com.os.domain.validation.ValidationGroups;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClienteInput {

	@NotNull(groups = ValidationGroups.ClienteId.class)
	private Long id;

	@NotBlank
	@Size(max = 50)
	private String nome;
	
	@NotBlank
	@Email
	private String email;

	@NotBlank
	@Size(max = 20)
	private String telefone;

}
