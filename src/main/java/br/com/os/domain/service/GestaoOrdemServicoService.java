package br.com.os.domain.service;

import java.time.OffsetDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.os.domain.exception.EntidadeNaoEncontradaException;
import br.com.os.domain.exception.NegocioException;
import br.com.os.domain.model.Cliente;
import br.com.os.domain.model.Comentario;
import br.com.os.domain.model.OrdemServico;
import br.com.os.domain.model.StatusOrdemServico;
import br.com.os.domain.repository.ClienteRepository;
import br.com.os.domain.repository.ComentarioRepository;
import br.com.os.domain.repository.OrdemServicoRepository;

@Service
public class GestaoOrdemServicoService {

	@Autowired
	private OrdemServicoRepository ordemServicoRepository;
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private ComentarioRepository comentarioRepository;

	public OrdemServico criar(OrdemServico ordemServico) {
		Long idCliente = ordemServico.getCliente().getId();
		Cliente cliente = clienteRepository.findById(idCliente).orElseThrow(() -> new NegocioException("Cliente não encontrado"));
		
		ordemServico.setCliente(cliente);
		ordemServico.setStatus(StatusOrdemServico.ABERTA);
		ordemServico.setDataAbertura(OffsetDateTime.now());

		return ordemServicoRepository.save(ordemServico);
	}
	
	public Comentario adicionarComentario(Long ordemServicoId, String descricao) {
		OrdemServico ordemServico = buscarOrdemServico(ordemServicoId);
		
		Comentario comentario = new Comentario();
		comentario.setDescricao(descricao);
		comentario.setOrdemServico(ordemServico);
		comentario.setDataEnvio(OffsetDateTime.now());
		
		return comentarioRepository.save(comentario);
	}
	
	public void finalizarOrdemServico(Long ordemServicoId) {
		OrdemServico ordemServico = buscarOrdemServico(ordemServicoId);
		ordemServico.finalizar();

		ordemServicoRepository.save(ordemServico);
	}

	public List<OrdemServico> buscarStatus(String status) {
		StatusOrdemServico value = validaStatus(status);
		List<OrdemServico> listaDeOs = ordemServicoRepository.findByStatus(value);

		if (listaDeOs.isEmpty()) {
			throw new EntidadeNaoEncontradaException("Não foram encontradas ordens de serviço com esse status");
		}

		return listaDeOs;
	}

	private OrdemServico buscarOrdemServico(Long ordemServicoId) {
		return ordemServicoRepository.findById(ordemServicoId).orElseThrow(() -> new EntidadeNaoEncontradaException("Ordem de serviço não encontrada"));
	}

	private StatusOrdemServico validaStatus(String status) {
		try {
			StatusOrdemServico value = StatusOrdemServico.valueOf(status);
			return value;
		} catch (IllegalArgumentException e) {
			throw new NegocioException("Somente existe os status aberta, finalizada e cancelada");
		}
	}

}
